//
//  ViewController.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, UICollectionViewDelegate,
                          UICollectionViewDataSource, GameEngineDelegate {
    
    var level: Level = .Beginner
    var gameEngine: GameEngine!
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        resetGame()
        
    }
    
    func resetGame() {
        
        gameEngine = GameEngine(level)
        gameEngine.delegate = self
        
        updateCollectionViewBounds()
        
        collectionView.reloadData()
    }
    
    private func updateCollectionViewBounds() {
    
        let width = gameEngine.boardSize() * 32
        let height = gameEngine.boardSize() * 32
    
        collectionView.frame.size = CGSize(width: width, height: height)
        collectionView.center = view.center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

  
    @IBAction func setBeginnerLevel(sender: UIButton) {
    
        level = .Beginner
        resetGame()
    }
    
    @IBAction func setIntermediateLevel(sender: UIButton) {
        
        level = .Intermediate
        resetGame()
        
    }

    @IBAction func setAdvancedLevel(sender: UIButton) {
        
        level = .Advanced
        resetGame()
        
    }
    
    
    func alertGameEnd() {
        
        let alert = UIAlertController(title: "Congrats!", message: "You beat us!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,
                                      handler: {(alert: UIAlertAction!) in self.resetGame() }) )
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertMineFound() {
        
        let alert = UIAlertController(title: "Game Over!", message: "You bumped on a mine!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,
                                      handler: {(alert: UIAlertAction!) in self.resetGame() }) )
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    /* Collection View delegates creates grid based on gameEngine */
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return gameEngine.boardSize()
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return gameEngine.boardSize()
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TileCell",
                                                      for: indexPath) as! TileCell
        
        cell.setup(tile: gameEngine.getTile(indexPath.section, indexPath.row)!)
        
        return cell
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        gameEngine.revealTile(indexPath.section, indexPath.row)
        collectionView.reloadItems(at: [indexPath] )
        
    }
    
    
    /* Game Engine Delegate - Called everytime a tile is automatically revealed or the game state changes */
    
    func gameEngine(revealed tile: Tile) {
        
        collectionView.reloadItems(at: [ IndexPath(row: tile.col, section: tile.row) ] )
        
    }
    
    func gameEngine(updated gameState: GameState) {
        
        if( gameEngine.gameState == .MineFound ) {
            alertMineFound()
        }
        
        if( gameEngine.gameState == .Complete) {
            alertGameEnd()
        }

    }

}


//
//  SimpleTilesGenerator.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

class SimpleTilesGenerator : TilesGenerator {
    
    let tilesPattern : [[Int]]
    
    init(_ tilesPattern: [[Int]] ) {
        
        self.tilesPattern = tilesPattern
        
    }

    override func generateTiles(size: Int) -> [[Tile]] {
        
        var board:[[Tile]] = []
        
        for row in 0..<tilesPattern.count {
            
            var tiles:[Tile] = []
            
            for col in 0 ..< tilesPattern[row].count {
                
                tiles.append(Tile(col: col, row: row, isMine: (tilesPattern[row][col] == 1) ))
                
            }
            
            board.append(tiles)
        }
        
        return board
    }
    
}

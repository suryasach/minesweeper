//
//  GameEngine.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

enum GameState {
    
    case NotStarted
    case InProgress
    case Complete
    case MineFound
    
}

protocol GameEngineDelegate {
 
    func gameEngine(revealed tile: Tile)
    func gameEngine(updated gameState: GameState)
    
}

class GameEngine {
    
    var level: Level
    var board: MinesBoard?
    var delegate: GameEngineDelegate?
    private var _gameState: GameState = .NotStarted
 
    init(_ level: Level) {
        
        self.level = level
        reset(level)
        
    }
    
    public internal(set) var gameState: GameState {
        get {
            return self._gameState
        }
        set {
            self._gameState = newValue
            delegate?.gameEngine(updated: gameState)
        }
    }
    
    func reset(_ level: Level) {
        
        self.level = level
        board = MinesBoard(size: level.boardSize())
        gameState = .InProgress
        
    }
    
    func boardSize() -> Int {
        
        return level.boardSize()
        
    }
    
    func getTile(_ row: Int, _ col: Int) -> Tile? {
        
        return board?.getTile(row, col)
        
    }

    func revealTile(_ row: Int, _ col: Int) {
        
        let tile = getTile(row, col)!
        
        if tile.isRevealed {
            return
        }
        
        tile.isRevealed = true
        
        switch( tile ) {
            
            case _ where tile.isMine :

                                    gameState = .MineFound

            case _ where tile.minesNearby == 0 :
            
                                    revealNeighbours(tile: tile)
                                    updateGameState()
            
            default:
                                    updateGameState()
        }

    }
    
    private func revealNeighbours(tile: Tile) {
        
        board?.neighboursOf(tile: tile).forEach {
            
            tile in
            
            if ( !tile.isRevealed ) {
                
                tile.isRevealed = true
                self.delegate?.gameEngine(revealed: tile)
                
                if tile.minesNearby == 0  {
                    revealNeighbours( tile: tile )
                }
                
            }
            
        }
        
    }
    
    private func updateGameState() {
        
        for row in 0..<boardSize() {
            
            for col in 0..<boardSize() {
                
                let tile = getTile(row, col)
                if( tile?.isRevealed != true && tile?.isMine != true ) {
                    return
                }
            }
        }
        
        gameState = .Complete

    }
    
    
}

//
//  Level.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

enum Level: Int {
    
    case Beginner = 0
    case Intermediate = 1
    case Advanced = 2
    
    func boardSize() -> Int {
        
        switch( self ) {
            
            case .Beginner: return 4
            
            case .Intermediate: return 8
            
            case .Advanced: return 12
            
        }
        
    }
    
}

//
//  LevelTests.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation
import XCTest

class LevelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testBeginner() {
        
        let level = Level.Beginner
        
        XCTAssert(level.boardSize() == 4)
        
    }
    
    func testAdvanced() {
        
        let level = Level.Advanced
        
        XCTAssert(level.boardSize() == 12)
        
    }
    
    func testIntermediate() {
        
        let level = Level.Intermediate
        
        XCTAssert(level.boardSize() == 8)
        
    }
    
    func testGameEngineLevel() {
        
        var gameEngine = GameEngine(.Beginner)
        
        XCTAssert(gameEngine.boardSize() == 4)
        
        gameEngine = GameEngine(.Advanced)
        
        XCTAssert(gameEngine.boardSize() == 12)
        
        gameEngine = GameEngine(.Intermediate)
        
        XCTAssert(gameEngine.boardSize() == 8)
        
    }
}

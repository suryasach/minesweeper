//
//  MinesBoardTests.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation
import XCTest

class MinesBoardTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInit() {
        
        let board = MinesBoard(size: 5)
        
        XCTAssert(board.getTile(0, 0) != nil)
        XCTAssert(board.getTile(2, 4) != nil)

        XCTAssert(board.getTile(10, 10) == nil)
        XCTAssert(board.getTile(-1, 2) == nil)
        XCTAssert(board.getTile(1, 10) == nil)
        XCTAssert(board.getTile(10, 1) == nil)
        XCTAssert(board.getTile(2, 5) == nil)
        XCTAssert(board.getTile(5, 2) == nil)

    }
    
    func testNearByCalculation8Neighbours() {
        
        let tilesPattern = [
         [ 1, 1, 1, 1 ],
         [1, 0, 1, 0],
         [1, 1, 1, 0],
         [0, 0, 0 , 0]
         ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()

        XCTAssert(board.getTile(1, 1)?.minesNearby == 8)

    }
    
    func testNearByCalculation7Neighbours() {
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [1, 0, 0, 0],
            [1, 1, 1, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 7)
        
    }
    
    
    func testNearByCalculation6Neighbours() {
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [1, 0, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 6)
        
    }
    
    func testNearByCalculation5Neighbours() {
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [1, 0, 0, 0],
            [1, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 5)
        
    }
    
    func testNearByCalculation4Neighbours() {
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 4)
        
    }
    
    func testNearByCalculation3Neighbours() {
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 3)
        
    }

    
    func testNearByCalculation2Neighbours() {
        
        let tilesPattern = [
            [ 0, 1, 1, 1 ],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 2)
        
    }
    
    
    func testNearByCalculation1Neighbours() {
        
        let tilesPattern = [
            [ 0, 0, 1, 1 ],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 1)
        
    }
    
    func testNearByCalculation0Neighbours() {
        
        let tilesPattern = [
            [ 0, 0, 0, 1 ],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        let board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()
        
        XCTAssert(board.getTile(1, 1)?.minesNearby == 0)
        
    }
}

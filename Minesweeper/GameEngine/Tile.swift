//
//  Square.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

class Tile {
    
    var isRevealed = false
    var minesNearby = 0
    
    let col: Int
    let row: Int
    let isMine: Bool
    
    init(col: Int, row: Int, isMine: Bool) {
        
        self.col = col
        self.row = row
        self.isMine = isMine

    }
    
}

//
//  MinesBoard.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

class MinesBoard {
    
    let size: Int
    var board: [[Tile]] = []
    let neighboursOffset = [(-1, -1), (0, -1), (1, -1),
                      (-1, 0),           (1, 0),
                      (-1, 1),  (0, 1), (1, 1)]
    var tilesGenerator = TilesGenerator()
    
    
    init( size: Int ) {
        
        self.size = size
        resetBoard()
        
    }

    func resetBoard() {
        
        let testMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if( testMode ) {
            //Mocking just one know board.
            board = SimpleTilesGenerator([
                [ 1, 0, 0, 0 ],
                [0, 0, 1, 0],
                [0, 0, 0, 0],
                [0, 0, 0 , 0]
                ]
                ).generateTiles(size: size)

        } else {
            board = tilesGenerator.generateTiles(size: size)
        }
        
        updateMinesNearby()
        
    }
    
    func getTile( _ row: Int , _ col: Int  ) -> Tile? {
        
        if isValidTile(row, col) {
            
            return board[row][col]
        }
        
        return nil
    }
        
    func neighboursOf(tile: Tile) -> [Tile] {
        
        var tiles:[Tile] = []
        
        for (rowOffset, colOffset) in neighboursOffset {
            
            let row = tile.row + rowOffset
            let col = tile.col + colOffset
            
            if let neighbour = getTile(row, col) {
                
                tiles.append(neighbour)
                
            }
        
        }
        
        return tiles
    }

    private func updateMinesNearby() {
        
        board.forEach { tiles in
            tiles.forEach { tile in
                updateMinesNearby(tile: tile)
            }
        }

    }
    
    private func isValidTile( _ row: Int , _ col: Int ) -> Bool {
    
        return (row >= 0 && row < size) && (col >= 0 && col < size)
        
    }
    
    private func updateMinesNearby(tile: Tile) {
        
        tile.minesNearby = 0
        
        for neighbour in neighboursOf(tile: tile) {
            
            if ( neighbour.isMine ) {
                
                tile.minesNearby += 1

            }
            
        }
        
    }
    
}

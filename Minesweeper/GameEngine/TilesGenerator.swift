//
//  TilesGenerator.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation

class TilesGenerator {
    
    /* Default Tiles Generator - Randomized mines probability of 1 in 10 */
    func generateTiles(size: Int) -> [[Tile]] {
        
        var board:[[Tile]] = []
        
        for row in 0 ..< size {
            var tiles:[Tile] = []
            for col in 0 ..< size {
                let tile = Tile(col: col, row: row, isMine: ((arc4random() % 10) == 0) )
                tiles.append(tile)
            }
            board.append(tiles)
        }
        
        return board
    }
    
}

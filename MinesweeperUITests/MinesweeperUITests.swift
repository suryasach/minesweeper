//
//  MinesweeperUITests.swift
//  MinesweeperUITests
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import XCTest

class MinesweeperUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBeginnner() {

        let app = XCUIApplication()
        //app.launchArguments = ["testMode"]
        app.launch()
        
        
        XCTAssertEqual(app.collectionViews.cells.count, 16)

    }
    
    func testBeginnnerReset() {
        
        let app = XCUIApplication()
        //app.launchArguments = ["testMode"]
        app.launch()
        
        app.buttons["Beginner"].tap()
        
        XCTAssertEqual(app.collectionViews.cells.count, 16)
        
    }
    
    func testIntermediate() {

        let app = XCUIApplication()
        //app.launchArguments = ["testMode"]
        app.launch()
        
        app.buttons["Intermediate"].tap()

        XCTAssertEqual(app.collectionViews.cells.count, 64)
 
    }
    
    
    func testAdvanced() {
        
        let app = XCUIApplication()
        //app.launchArguments = ["testMode"]
        app.launch()
        
        app.buttons["Advanced"].tap()
        
        XCTAssertEqual(app.collectionViews.cells.count, 144)

        
    }
    
    func testTapMine() {
    
        let app = XCUIApplication()
        app.launchArguments = ["testMode"]
        app.launch()
        
        XCTAssertEqual(app.collectionViews.cells.count, 16)
        
        app.collectionViews.children(matching: .cell).element(boundBy: 0).tap()
        
        let alert = app.alerts["Game Over!"]

        XCTAssert(alert.exists)

    }
    
    func testCompletion() {
        
        let app = XCUIApplication()
        app.launchArguments = ["testMode"]
        app.launch()
        
        XCTAssertEqual(app.collectionViews.cells.count, 16)
        
    
        app.collectionViews.children(matching: .cell).element(boundBy: 15).tap()
        app.collectionViews.children(matching: .cell).element(boundBy: 1).tap()
        app.collectionViews.children(matching: .cell).element(boundBy: 2).tap()
        app.collectionViews.children(matching: .cell).element(boundBy: 3).tap()
        app.collectionViews.children(matching: .cell).element(boundBy: 4).tap()
        app.collectionViews.children(matching: .cell).element(boundBy: 7).tap()

        
        let alert = app.alerts["Congrats!"]
 
        XCTAssert(alert.exists)
        
    }
    
}

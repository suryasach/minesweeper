//
//  GameEngineTests.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import XCTest

class GameEngineMockDelegate: GameEngineDelegate {
    
    var gameStateCalled = 0
    var revealTilesCalled = 0
    
    func gameEngine(revealed tile: Tile) {
        revealTilesCalled += 1
    }
    func gameEngine(updated gameState: GameState) {
        gameStateCalled += 1
    }
}

class GameEngineTests: XCTestCase {
    
    var gameEngine: GameEngine!
    var delegate: GameEngineMockDelegate!

    override func setUp() {
        
        super.setUp()
        
        let tilesPattern = [
            [ 1, 0, 1, 1 ],
            [1, 0, 1, 0],
            [0, 1, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)

        gameEngine = GameEngine(.Beginner)
        gameEngine.board?.tilesGenerator = tilesGenerator
        gameEngine.board?.resetBoard()
        
        delegate = GameEngineMockDelegate()
        gameEngine.delegate = delegate
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testRevealMineFirstMove() {
       
        gameEngine.revealTile(0, 0)
        
        XCTAssert(delegate.gameStateCalled == 1)
        XCTAssert(gameEngine.gameState == .MineFound )
        
    }
    
    func testRevealMineNthMove() {
        
        XCTAssert(gameEngine.getTile(0, 1)?.isRevealed == false )

        gameEngine.revealTile(0, 1)
        
        XCTAssert(delegate.gameStateCalled == 0)
        XCTAssert(gameEngine.gameState == .InProgress )
        XCTAssert(gameEngine.getTile(0, 1)?.isRevealed == true )

        gameEngine.revealTile(0, 0)

        XCTAssert(delegate.gameStateCalled == 1)
        XCTAssert(gameEngine.gameState == .MineFound )
        
    }
    
    func testRevealRegularTile() {
        
        XCTAssert(gameEngine.getTile(0, 1)?.isRevealed == false )
        
        gameEngine.revealTile(0, 1)
        
        XCTAssert(delegate.gameStateCalled == 0)
        XCTAssert(gameEngine.gameState == .InProgress )
        
        for row in 0 ..< 4 {
            
            for col in 0 ..< 4 {
                
                XCTAssert(gameEngine.getTile(row, col)?.isRevealed == ( row == 0 && col == 1 ) )

            }
            
        }

    }
    
    func testRevealZero() {
        
        XCTAssert(gameEngine.getTile(3, 3)?.isRevealed == false )
        
        gameEngine.revealTile(3, 3)
        
        XCTAssert(delegate.gameStateCalled == 0)
        XCTAssert(gameEngine.gameState == .InProgress )
        
        XCTAssert(gameEngine.getTile(3, 3)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(3, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(2, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(2, 3)?.isRevealed == true )
        
        XCTAssert(gameEngine.getTile(3, 1)?.isRevealed == false )
        XCTAssert(gameEngine.getTile(2, 1)?.isRevealed == false )
        XCTAssert(gameEngine.getTile(1, 3)?.isRevealed == false )

    }
    
    func testRevealRecursiveZero() {
        
        let tilesPattern = [
            [ 1, 0, 0, 0 ],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        gameEngine = GameEngine(.Beginner)
        gameEngine.board?.tilesGenerator = tilesGenerator
        gameEngine.board?.resetBoard()
        
        delegate = GameEngineMockDelegate()
        gameEngine.delegate = delegate
        
        gameEngine.revealTile(3, 3)
        
        XCTAssert(gameEngine.getTile(0, 0)?.isRevealed == false )
        XCTAssert(gameEngine.getTile(0, 1)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(0, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(0, 3)?.isRevealed == true )

        XCTAssert(gameEngine.getTile(1, 0)?.isRevealed == false )
        XCTAssert(gameEngine.getTile(1, 1)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(1, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(1, 3)?.isRevealed == true )
        
        XCTAssert(gameEngine.getTile(2, 0)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(2, 1)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(2, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(2, 3)?.isRevealed == true )
        
        XCTAssert(gameEngine.getTile(3, 0)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(3, 1)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(3, 2)?.isRevealed == true )
        XCTAssert(gameEngine.getTile(3, 3)?.isRevealed == true )
    }
    
    func testRevealGameCompletionOnRecursion() {
        
        let tilesPattern = [
            [ 1, 0, 0, 0 ],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        gameEngine = GameEngine(.Beginner)
        gameEngine.board?.tilesGenerator = tilesGenerator
        gameEngine.board?.resetBoard()
        
        delegate = GameEngineMockDelegate()
        gameEngine.delegate = delegate
        
        gameEngine.revealTile(3, 3)

        XCTAssert(delegate.gameStateCalled == 1)
        XCTAssert(gameEngine.gameState == .Complete )

    }
    
    func testRevealGameCompletion() {
        
        let tilesPattern = [
            [ 1, 0, 0, 0 ],
            [0, 0, 1, 0],
            [0, 0, 0, 0],
            [0, 0, 0 , 0]
        ]
        
        /*
 
 [ X, 1, 1, 1 ],
 [1, 2, X, 1],
 [0, 1, 1, 1],
 [0, 0, 0 , 0]
 
 */
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        gameEngine = GameEngine(.Beginner)
        gameEngine.board?.tilesGenerator = tilesGenerator
        gameEngine.board?.resetBoard()
        
        delegate = GameEngineMockDelegate()
        gameEngine.delegate = delegate
        
        gameEngine.revealTile(3, 3)
        XCTAssert(delegate.gameStateCalled == 0)

        gameEngine.revealTile(0, 1)
        XCTAssert(delegate.gameStateCalled == 0)

        gameEngine.revealTile(0, 2)
        XCTAssert(delegate.gameStateCalled == 0)

        gameEngine.revealTile(0, 3)
        XCTAssert(delegate.gameStateCalled == 0)

        gameEngine.revealTile(1, 0)
        XCTAssert(delegate.gameStateCalled == 0)

        gameEngine.revealTile(1, 3)

        XCTAssert(delegate.gameStateCalled == 1)
        XCTAssert(gameEngine.gameState == .Complete )
        
    }

    
}

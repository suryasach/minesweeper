//
//  TileCell.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import Foundation
import UIKit

class TileCell: UICollectionViewCell {
    
    @IBOutlet var label: UILabel!
    
    func setup(tile: Tile) {
        
        if tile.isMine {
            label.text = "X"
        } else{
            label.text = tile.minesNearby == 0 ? "" : String(tile.minesNearby)
        }
        
        if( tile.isRevealed ) {
            self.backgroundColor = UIColor.lightGray
        } else {
            self.backgroundColor = UIColor.darkGray
        }
        
        label.isHidden = !tile.isRevealed

    }
}

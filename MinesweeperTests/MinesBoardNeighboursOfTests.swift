//
//  MinesBoardNeighboursOfTests.swift
//  Minesweeper
//
//  Created by Suresh on 22/11/16.
//  Copyright © 2016 Suresh. All rights reserved.
//

import XCTest

class MinesBoardNeighboursOfTests: XCTestCase {
    
    var board = MinesBoard(size: 4)
    
    override func setUp() {
        
        super.setUp()
        
        let tilesPattern = [
            [ 1, 1, 1, 1 ],
            [1, 0, 1, 0],
            [1, 1, 1, 0],
            [0, 0, 0 , 0]
        ]
        
        let tilesGenerator =  SimpleTilesGenerator(tilesPattern)
        
        board = MinesBoard(size: 4)
        board.tilesGenerator = tilesGenerator
        board.resetBoard()

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testNeighboursOfCorners() {
        
        XCTAssert(board.neighboursOf(tile: board.getTile(0, 0)!).count == 3)
        XCTAssert(board.neighboursOf(tile: board.getTile(3, 3)!).count == 3)
        XCTAssert(board.neighboursOf(tile: board.getTile(3, 0)!).count == 3)
        XCTAssert(board.neighboursOf(tile: board.getTile(0, 3)!).count == 3)

    }
    
    func testNeighboursOfEdges() {
        
        
        XCTAssert(board.neighboursOf(tile: board.getTile(2, 0)!).count == 5)
        XCTAssert(board.neighboursOf(tile: board.getTile(0, 2)!).count == 5)

    }
    
    func testNeighboursOfCenter() {
        
        
        XCTAssert(board.neighboursOf(tile: board.getTile(2, 2)!).count == 8)
        
    }


    
}
